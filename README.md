# WordPress instance for the Superior website

## Installation

### Nginx

Run:

    sudo apt-get install nginx
    sudo rm /etc/nginx/sites-enabled/default

Edit `/etc/nginx/sites-available/superior-website`:

    server {
        listen 80;

        location / {
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   Host      $http_host;
            proxy_pass         http://127.0.0.1:3000;
        }
    }

Run:

    sudo ln -s /etc/nginx/sites-available/superior-website /etc/nginx/sites-enabled/superior-website
    sudo service nginx restart

### Install Docker

Run:

    sudo apt-get update
    sudo apt-get install curl
    curl -fsSL https://get.docker.com -o get-docker.sh
    sudo sh get-docker.sh
    sudo usermod -aG docker ssnead

Docker compose:

    sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose


### Project

1. Install Docker on a Linux server.
2. Install Docker Compose.
3. Clone repository.
4. Run `docker-compose up -d`
5. Install Nginx and add the following entry for the available website:

    server {
        listen 80;

        location / {
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   Host      $http_host;
            proxy_pass         http://127.0.0.1:3000;
        }
    }

6. Run `docker-compose exec wordpress chown -R www-data /var/www/html`
7. Go on website and configure Wordpress using the wizard.
